import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
  <h1>New React Application</h1>
  <h2>Version 1</h2>
      </header>

    </div>
  );
}

export default App;
