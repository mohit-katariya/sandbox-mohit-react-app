#pull official base image
FROM node:14.16.0-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install

# add app
COPY . ./

# start app
CMD ["npm", "start"] 


# FROM node:latest

# RUN mkdir -p app/src

# WORKDIR /app/src

# COPY package.json .

# RUN npm install

# COPY . .

# EXPOSE 8082

# CMD ["npm","start"]